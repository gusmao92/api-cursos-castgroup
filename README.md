# CRUD CURSOS
## CastGroup

Criação de API para avaliação

Para o desenvolvimento foi utilizado o banco de dados H2

* Acesso ao banco de memória M2

```
_Driver Class:_ org.h2.Driver

_JDBC URL:_ jdbc:h2:mem:testdb

_User Name:_ sa
```
```
http://localhost:8080/h2-console
```
Toda a API está documentada via SWAGGER

* Acesso ao SWAGGER
```
http://localhost:8080/swagger-ui.html#
```
