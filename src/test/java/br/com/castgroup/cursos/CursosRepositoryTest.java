package br.com.castgroup.cursos;

import br.com.castgroup.cursos.model.Categoria;
import br.com.castgroup.cursos.model.Curso;
import br.com.castgroup.cursos.repository.CategoriaRepository;
import br.com.castgroup.cursos.repository.CursoRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CursosRepositoryTest {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createCursoPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");

        categoriaRepository.save(categoria);

        Curso curso =
                new Curso(1,"Curso Java Web Full-Stack", new Date(2020, 04, 20), new Date(2020, 04, 25), 10, categoria);

        cursoRepository.save(curso);

        Assertions.assertThat(curso.getId()).isNotNull();
    }

    @Test
    public void deleteCursoPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");

        categoriaRepository.save(categoria);

        Curso curso =
                new Curso(1,"Curso Java Web Full-Stack", new Date(2020, 04, 20), new Date(2020, 04, 25), 10, categoria);

        cursoRepository.delete(curso);

        Assertions.assertThat(cursoRepository.findById(categoria.getId())).isNotNull();
    }

    @Test
    public void updateCursoPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");

        categoriaRepository.save(categoria);

        Curso curso =
                new Curso(1, "Curso Java Web Full-Stack", new Date(2020, 04, 20), new Date(2020, 04, 25), 10, categoria);

        cursoRepository.save(curso);

        curso.setDescricao("UPDATE TESTE");
        curso.setDt_inicio(new Date(2020, 04, 20));
        curso.setDt_fim(new Date(2020, 04, 25));
        curso.setQt_aluno(10);
        curso.setCategoria(categoria);

        cursoRepository.save(curso);

        Assertions.assertThat(curso.getId()).isNotNull();
        Assertions.assertThat(curso.getDescricao()).isEqualTo("UPDATE TESTE");
    }
}
