package br.com.castgroup.cursos;

import br.com.castgroup.cursos.model.Categoria;
import br.com.castgroup.cursos.repository.CategoriaRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoriaRepositoryTest {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createCategoriaPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");

        categoriaRepository.save(categoria);

        Assertions.assertThat(categoria.getId()).isNotNull();
        Assertions.assertThat(categoria.getDescricao()).isEqualTo("CATEGORIA TESTE");
    }

    @Test
    public void deleteCategoriaPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");

        categoriaRepository.delete(categoria);

        Assertions.assertThat(categoriaRepository.findById(categoria.getId())).isNotNull();
    }

    @Test
    public void updateCategoriaPersistData() {

        Categoria categoria = new Categoria(100, "CATEGORIA TESTE");
        categoriaRepository.save(categoria);

        categoria.setId(100);
        categoria.setDescricao("CATEGORIA UPDATE");

        categoriaRepository.save(categoria);

        Assertions.assertThat(categoria.getId()).isNotNull();
        Assertions.assertThat(categoria.getId()).isEqualTo(100);
        Assertions.assertThat(categoria.getDescricao()).isEqualTo("CATEGORIA UPDATE");
    }
}
