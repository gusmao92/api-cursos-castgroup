package br.com.castgroup.cursos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CursoNotFoundException extends RuntimeException {
	public CursoNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
