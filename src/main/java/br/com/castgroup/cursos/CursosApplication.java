package br.com.castgroup.cursos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.TimeZone;

@SpringBootApplication
public class CursosApplication {

	@PostConstruct
	public void init(){
		TimeZone.setDefault(TimeZone.getTimeZone("GMT-3"));   // It will set UTC timezone
	}

	public static void main(String[] args) {
		SpringApplication.run(CursosApplication.class, args);
	}

}
