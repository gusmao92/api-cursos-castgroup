package br.com.castgroup.cursos.service;

import br.com.castgroup.cursos.exception.CursoNotFoundException;
import br.com.castgroup.cursos.model.Categoria;
import br.com.castgroup.cursos.repository.CategoriaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class CategoriaService {

    @Autowired
    CategoriaRepository categoriaRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public List<Categoria> listarCategorias() {

        LOGGER.info("Listando categorias");

        return categoriaRepository.findAll();
    }

    public Optional<Categoria> listarCategoriaById(int id) {

        LOGGER.info("Listando categoria ID: " + id);

        Optional<Categoria> categoria =  categoriaRepository.findById(id);

        if(!categoria.isPresent())
            throw new CursoNotFoundException("id: " + id);

        return categoriaRepository.findById(id);
    }

    public void deletarCategoria(int id) {

        LOGGER.info("Deletando categoria ID: " + id);

        // CHAMADA PARA VALIDAR A EXISTÊNCIA DA CATEGORIA
        listarCategoriaById(id);

        categoriaRepository.deleteById(id);
    }

    public void inserirCategoria(Categoria categoria) {

        LOGGER.info("Inserindo categoria: " + categoria.getDescricao());

        categoriaRepository.save(categoria);
    }

    public void atualizarCategoria(int id, Categoria categoria) {

        LOGGER.info("Atualizando categoria: " + categoria.getDescricao());

        categoria.setId(id);

        categoriaRepository.save(categoria);
    }
}
