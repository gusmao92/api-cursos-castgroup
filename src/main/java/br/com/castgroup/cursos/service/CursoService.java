package br.com.castgroup.cursos.service;

import br.com.castgroup.cursos.exception.CursoBadRequestException;
import br.com.castgroup.cursos.exception.CursoNotFoundException;
import br.com.castgroup.cursos.model.Curso;
import br.com.castgroup.cursos.repository.CursoRepository;
import br.com.castgroup.cursos.util.CursoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CursoService {

    @Autowired
    CursoRepository cursoRepository;

    @Autowired
    CursoUtil cursoUtil;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public List<Curso> listarCursos() {

        LOGGER.info("Listando cursos");

        return cursoRepository.findAll();
    }

    public Optional<Curso> listarCursoById(int id) {

        LOGGER.info("Listando curso ID: " + id);

        Optional<Curso> curso = cursoRepository.findById(id);

        if (!curso.isPresent())
            throw new CursoNotFoundException("id: " + id);

        return cursoRepository.findById(id);
    }

    public void deletarCurso(int id) {

        LOGGER.info("Deletando curso ID: " + id);

        listarCursoById(id);

        cursoRepository.deleteById(id);
    }

    public void inserirCurso(Curso curso) {

        LOGGER.info("Inserindo curso: " + curso.getDescricao());

        List<Curso> listaCurso = listarCursos();

        if (!listaCurso.isEmpty()) {

            if (!cursoUtil.validarPeriodo(listaCurso, curso))
                throw new CursoBadRequestException("Existe(m) curso(s) planejados(s) dentro do período informado.");
        }

        cursoRepository.save(curso);
    }

    public void atualizarCurso(int id, Curso curso) {

        LOGGER.info("Atualizando curso: " + curso.getDescricao());

        listarCursoById(id);

        curso.setId(id);
        cursoRepository.save(curso);
    }

}
