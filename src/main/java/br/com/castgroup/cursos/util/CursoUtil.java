package br.com.castgroup.cursos.util;

import br.com.castgroup.cursos.model.Curso;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class CursoUtil {

    public Boolean validarPeriodo(List<Curso> listaCursos, Curso novoCurso) {

        Date novo_dtInicio = novoCurso.getDt_inicio();

        return listaCursos.stream()
                .anyMatch(c -> novo_dtInicio.before(c.getDt_inicio()) || novo_dtInicio.after(c.getDt_fim()));

    }
}
