package br.com.castgroup.cursos.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Aluno {

    @Id
    @GeneratedValue
    private Integer id;
    private String nome;
    private String sobrenome;

    @ManyToOne
    @JoinColumn(name = "id", nullable = false)
    private Curso curso;

}
