package br.com.castgroup.cursos.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "curso_id")
    private Integer id;

    @NotNull(message = "Esse campo não pode ser NULL")
    @Size(max = 255, message = "A descrição do curso não deve conter mais de 255 caractéres")
    @ApiModelProperty(notes = "A descrição do curso não deve conter mais de 255 caractéres")
    private String descricao;

    @NotNull(message = "Esse campo não pode ser NULL")
    @FutureOrPresent(message = "A data de início deve ser maior ou igual a data atual")
    @ApiModelProperty(notes = "A data de início deve ser maior ou igual a data atual")
    private Date dt_inicio;

    @NotNull(message = "Esse campo não pode ser NULL")
    @Future(message = "A data de fim deve ser maior que a data atual")
    @ApiModelProperty(notes = "A data de fim deve ser maior que a data atual")
    private Date dt_fim;

    @Min(value = 0, message = "A quantidade de alunos deve ser maior que ZERO (0)")
    private Integer qt_aluno;

    @ManyToOne
    @JoinColumn(name = "id", nullable = false)
    private Categoria categoria;

}
