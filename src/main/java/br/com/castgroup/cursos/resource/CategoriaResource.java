package br.com.castgroup.cursos.resource;

import br.com.castgroup.cursos.exception.CursoNotFoundException;
import br.com.castgroup.cursos.model.Categoria;
import br.com.castgroup.cursos.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/categorias", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoriaResource {

    @Autowired
    CategoriaService categoriaService;

    @GetMapping
    public List<Categoria> listarCategorias() {
        return categoriaService.listarCategorias();
    }

    @GetMapping(value = "/{id}")
    public Optional<Categoria> listarCategoriaById(@PathVariable int id) {
        return categoriaService.listarCategoriaById(id);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable int id){
        categoriaService.deletarCategoria(id);
    }

    @PostMapping
    public void inserirCategoria(@RequestBody Categoria categoria) {
        categoriaService.inserirCategoria(categoria);
    }

    @PutMapping(("/{id}"))
    public void atualizarCategoria(@PathVariable int id, @RequestBody Categoria categoria) {
        categoriaService.atualizarCategoria(id, categoria);
    }

}
