package br.com.castgroup.cursos.resource;

import br.com.castgroup.cursos.model.Curso;
import br.com.castgroup.cursos.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/cursos", produces = MediaType.APPLICATION_JSON_VALUE)
public class CursosResource {

    @Autowired
    CursoService cursoService;

    @GetMapping
    public List<Curso> listarCursos() {
        return cursoService.listarCursos();
    }

    @GetMapping(value = "/{id}")
    public Optional<Curso> listarCursoById(@PathVariable int id) {
        return cursoService.listarCursoById(id);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable int id){
        cursoService.deletarCurso(id);
    }

    @PostMapping
    public void inserirCurso(@Valid @RequestBody Curso curso) {
        cursoService.inserirCurso(curso);
    }

    @PutMapping(("/{id}"))
    public void atualizarCurso(@PathVariable int id, @Valid @RequestBody Curso curso) {
        cursoService.atualizarCurso(id, curso);
    }

}
