package br.com.castgroup.cursos.repository;

import br.com.castgroup.cursos.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {}
