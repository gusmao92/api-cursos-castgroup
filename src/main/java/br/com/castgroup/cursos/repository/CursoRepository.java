package br.com.castgroup.cursos.repository;

import br.com.castgroup.cursos.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CursoRepository extends JpaRepository<Curso, Integer> {}
