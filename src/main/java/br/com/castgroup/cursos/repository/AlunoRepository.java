package br.com.castgroup.cursos.repository;

import br.com.castgroup.cursos.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlunoRepository extends JpaRepository<Aluno, Integer> {}
